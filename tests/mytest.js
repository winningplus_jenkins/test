let expect = require("chai").expect;

describe("Custom Array", function(){
    let a = [];
    beforeEach(function(){
        a = [];
    });
    it("should be able to push", function(){
        a.push("1");
        expect(a.length).to.equals(1);
    });
    it("should be able to push more than one", function(){
        a.push("1");
        a.push("3");
        expect(a.length).to.equals(2); 
    });
});
